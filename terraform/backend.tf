terraform {
  required_version = ">= 1.1.5"
  backend "s3" {
    encrypt = true
    region  = "us-east-1"
  }
}

