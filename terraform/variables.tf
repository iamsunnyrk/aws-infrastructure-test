variable "env" {
  type        = string
  description = "environment (Dev, test, prod)"
}

variable "lambda_function" {
  type        = string
}
