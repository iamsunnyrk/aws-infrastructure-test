resource "aws_lambda_function" "test_lambda" {
  filename         = "${path.module}/../function.zip"
  function_name    = "Hello_world_function-${var.env}"
  role             = aws_iam_role.iam_for_lambda.arn
  handler          = ".lambdas/main.handler"
  source_code_hash = filebase64sha256("${path.module}/../function.zip")

  runtime = "python3.7"

  environment {
    variables = {
      input = "world"
    }
  }
}